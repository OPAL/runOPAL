Script to launch OPAL simulations.  

See the [Wiki](https://gitlab.psi.ch/OPAL/src/wikis/runOPAL) and 'runOPAL.py --help' for usage details.