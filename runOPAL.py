#!/usr/bin/env python

'''
Top-level script to run a batch of OPAL runs.

Example:
runOPAL.py --test MX=4:4:8
'''

import sys
from runOPAL.runOPAL import main

if __name__ == "__main__":
    main(sys.argv[1:])